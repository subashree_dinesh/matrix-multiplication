#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<assert.h>
#include<time.h>
#include "Mat_Mult.c"
#include "batch.bat"

int main(int argc, char  **argv)
{
    
    char filename[100];
    strcpy(filename,argv[1]);
    
    printf("\n%s   \n\n",argv[1]);
    
    fptr = fopen(filename, "r");
    
    
    char ch;
    char c[10] , empty[1]="";
    int i, dimension=0, num=0;
    int d_arr[10];
    clock_t start,end;

    double CPU_time_used_ijk, CPU_time_used_ikj, CPU_time_used_jik, CPU_time_used_jki, CPU_time_used_kij, CPU_time_used_kji;


    FILE* fptr;
    fptr = fopen("/Users/subashreed/Downloads/McW/Matrix/160x160.txt", "r");


    // Reading the dimension of the input matrices form the test file
	for(dimension=0;dimension<4;)
	{
		ch=fgetc(fptr);
			if(isspace(ch))
			{
				d_arr[dimension]=num;
				num=0;
				dimension++;

			}
			else
			{
                //to check if the row or column vlaue is negative or 0
				if(ch == '-'){
					printf("Row or Column value should be Greater than 0");
					exit(0);
				}
                //else if(ch=='0'){
                    //printf("Row or column value cannot be zero");
				//}
				else{
				num =(num*10) +(ch-'0');
				}

			}
	}

	// storing dimensions of matrices
    int row_1=d_arr[0],col_1=d_arr[1],row_2=d_arr[2],col_2=d_arr[3];


    if(row_1<0==1||row_2<0==1||col_1<0==1||col_2<0==1)
        {
     	printf("\nTestcase Failed!");
        }

    assert(row_1>0);
	assert(col_1>0);
	assert(row_2>0);
	assert(col_2>0);

    printf("%d %d %d %d \n",row_1,col_1,row_2,col_2);


	//	Verifying if the dimensions of matrices are valid for matrix multiplication (c1==r2)
	if(col_1 != row_2){
		printf("Matrix multiplication is not possible");
		exit(0);
	}


 	//	Allocating memory for input and output matrices
 	int**  matrix_1 ;
	matrix_1 = (int **) malloc(sizeof(int *) * row_1);
	for(i=0; i<row_1; i++)
 		matrix_1[i] = (int *)malloc(sizeof(int) * col_1);

 	int**  matrix_2 ;
	matrix_2 = (int **) malloc(sizeof(int *) * row_2);
	for(i=0; i<row_2; i++)
 		matrix_2[i] = (int *)malloc(sizeof(int) * col_2);

 	int**  result ;
	result = (int **) malloc(sizeof(int *) * row_1);
	for(i=0; i<row_1; i++)
 		result[i] = (int *)malloc(sizeof(int) * col_2);


	//	Reading Matrix_1 values one by one from text file
	int m,n;
	
	for( m=0;m<row_1;m++)
	{
		for( n=0;n<col_1;)
		{
			ch=fgetc(fptr);

			if(isspace(ch))
			{
				num = atoi(c);
				//printf("%d ",atoi(c));
				matrix_1[m][n]=(atoi(c));
				strcpy(c , empty);
				n++;

			}
			else
			{
				strncat(c , &ch , 1);
			}
		}

	}


	printf("\n");

	//	Reading matrix_2 values one by one from text file

	for( m=0;m<row_2;m++)
	{
		for( n=0;n<col_2;)
		{
			ch=fgetc(fptr);

			if(isspace(ch))
			{
				num = atoi(c);
				//printf("%d ",num);
				matrix_2[m][n]=atoi(c);
				strcpy(c , empty);
				n++;

			}
			else
			{
				strncat(c , &ch , 1);
			}
		}

	}

	printf("\n");


	//	Reading values for resultant matrix from the test file

	for( m=0;m<row_1;m++)
	{
		for( n=0;n<col_2;)
		{
			ch=fgetc(fptr);

			if(isspace(ch))
			{
				num = atoi(c);
				//printf("%d ",num);
				result[m][n]=atoi(c);
				strcpy(c , empty);
				n++;

			}
			else
			{
				strncat(c , &ch , 1);
			}
		}

	}

	//	creating a matrix to store actual_result obtained from the Mat_Mult.c code

	int** actual_result;
	actual_result = (int **) malloc(sizeof(int *) * row_1);
	for(i=0; i<row_1; i++)
 		actual_result[i] = (int *)malloc(sizeof(int) * col_2);



	//  initializing all the values of the matrix to be 0 at the beginning
	for(m=0;m<row_1;m++)
	{
		for(n=0;n<col_2;n++)
		{
			actual_result[m][n]=0;
		}
	}


	start = clock();

	// calling function from Mat_Mult.c
	Matrix_Multiplication_ijk(row_1,col_1,row_2,col_2,matrix_1,matrix_2,actual_result);

	end = clock();

	CPU_time_used_ijk = ((double) (end - start)) / CLOCKS_PER_SEC * 1000 ;
	printf("time taken ijk= %f ", CPU_time_used_ijk);

    printf("\n");

	// To print time taken in Time.csv file
    FILE *fpt;

    fpt = fopen("/Users/subashreed/Downloads/McW/Matrix/Time.csv", "a");

    //fprintf(fpt,"dimension, ijk, ikj, jik, jki, kij, kji\n");

    fprintf(fpt,"%d, %lf\n", col_1, CPU_time_used_ijk);

    fclose(fpt);


    /*
    //print the actual_result from Martix_Multiplication_ijk
    for(m=0;m<row_1;m++)
	{
		for(n=0;n<col_2;n++)
		{
			printf("%d ", actual_result[m][n]);
		}
	}
	*/





	//	checking whether values of result and actual_result matrices are same
	int j;
	for( i=0;i<row_1;i++)
	{
		for( j=0;j<col_2;j++)
		{
			if(result[i][j]!=actual_result[i][j] )
			{
					printf("\nTestcase Failed!!\n");
				return 0;
			}

		}

	}
	printf("\nTestcase Passed!\n");


}