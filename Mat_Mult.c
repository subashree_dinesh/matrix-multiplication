void Matrix_Multiplication_ijk(int row_1,int col_1,int row_2,int col_2,int** matrix_1, int** matrix_2,int** result)
{
    int i , j , k;
 	for(i=0; i < row_1; i++)
	{
		for(j=0; j < col_2; j++)
		{
	 		for(k=0; k<col_1; k++)
				result[i][j] += matrix_1[i][k] * matrix_2[k][j];
		}
	}

}


void Matrix_Multiplication_ikj(int row_1,int col_1,int row_2,int col_2,int** matrix_1, int** matrix_2,int** result)
{
    int i , j , k;
 	for(i=0; i < row_1; i++)
	{
		for(k=0; k < col_2; k++)
		{
	 		for(j=0; j<col_1; j++)
				result[i][k] += matrix_1[i][j] * matrix_2[j][k];
		}
	}

}


void Matrix_Multiplication_jik(int row_1,int col_1,int row_2,int col_2,int** matrix_1, int** matrix_2,int** result)
{

    int i , j , k;
 	for(j=0; j < row_1; j++)
	{
		for(i=0; i < col_2; i++)
		{
	 		for(k=0; k<col_1; k++)
				result[j][i] += matrix_1[j][k] * matrix_2[k][i];
		}
	}

}


void Matrix_Multiplication_jki(int row_1,int col_1,int row_2,int col_2,int** matrix_1, int** matrix_2,int** result)
{

    int i , j , k;
 	for(j=0; j < row_1; j++)
	{
		for(k=0; k < col_2; k++)
		{
	 		for(i=0; i<col_1; i++)
				result[j][k] += matrix_1[j][i] * matrix_2[i][k];
		}
	}

}


void Matrix_Multiplication_kij(int row_1,int col_1,int row_2,int col_2,int** matrix_1, int** matrix_2,int** result)
{

    int i , j , k;
 	for(k=0; k < row_1; k++)
	{
		for(i=0; i < col_2; i++)
		{
	 		for(j=0; j<col_1; j++)
				result[k][i] += matrix_1[k][j] * matrix_2[j][i];
		}
	}

}


void Matrix_Multiplication_kji(int row_1,int col_1,int row_2,int col_2,int** matrix_1, int** matrix_2,int** result)
{
    int i , j , k;
 	for(k=0; k < row_1; k++)
	{
		for(j=0; j < col_2; j++)
		{
	 		for(i=0; i<col_1; i++)
				result[k][j] += matrix_1[k][i] * matrix_2[i][j];
		}
	}

}
