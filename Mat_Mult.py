

import numpy as np
from numpy import *
from numpy import array
import os
import sys
import time
from MM import *
fn = sys.argv[1] 

#filename passed as argument
with open(fn) as file:                      #reading a file
    s=[]
    c=0
    for line in file:
        c+=1
        # print(type(line))
        li=(list(line.split(" ")))
        #print(li)
        
    
        for i in range(0, len(li)):
            if(li[i]=='\n'):
                li=li[:i]
            else:
                li[i] = int(li[i])
    
        s.append(li)

dim=s[0].copy()

if(dim[0]<=0 or dim[1]<=0 or dim[2]<=0 or dim[3]<=0):
        print("negative indexing")
        sys.exit()
lst1=s[1].copy()
print("\n")

lst2=s[2].copy()

lst3=s[3].copy()

#converting a list to array
arr1=np.array(lst1)
print(arr1)
arr2=array(lst2)
arr3=array(lst3)

#converting a array into matrix
mat1=arr1.reshape(dim[0],dim[1])
print(mat1)
mat2=arr2.reshape(dim[2],dim[3])
print(mat2)
mat3=arr3.reshape(dim[0],dim[3])
print(mat3)

start_time=time.time()                       #storing the start time
actual_result=MatMul(mat1,mat2)              #calling matrix mul function    
end_time=time.time()                         #storing the end time            

comparison= actual_result== mat3 
 # comparing two matrices   
equal_matrix=comparison.all()

print(actual_result)
if(equal_matrix):
        print("passed")
else:
         print("failed")
    
time_spent=double(end_time-start_time)       # calculating the time spent
print("time taken is",time_spent*1000)







